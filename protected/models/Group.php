<?php

/**
 * Group
 * @property integer $id
 * @property string $name
 * @property boolean $is_deleted
 * @property integer $parent_id
 */
class Group extends CActiveRecord
{
	protected $_children;

	/**
	 * Returns the static model of the specified AR class.
	 * @return static the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'group';
	}

	/**
	 * get groups in structured form
	 *  
	 * @param  boolean $only_parent if true the function return only the parent groups
	 * @return array return groups 
	 */
	public function getGroups($only_parent = false)
	{
		if(!$only_parent) {
			return $this->getGroupsArray();
		}
		
		$parent_groups = Yii::app()->db->createCommand()
	    ->select('id, name')
	    ->from('group')
	    ->where("is_deleted = :is_deleted and parent_id is NULL", [ ':is_deleted' => 0 ])
	    ->queryAll();
	 
		return array_map(function($item) {
				    	$item['level'] = 0;
				    	return $item;
				    }, $parent_groups);

	}

	/**
	 * return groups in hierarchy
	 * @param  array   $elements groups objects
	 * @param  integer  $parent_id 
	 * @param  integer $level  
	 * @return array    return groups in hierarchy
	 */
	public function getGroupsArray(array $elements = [], $parent_id = NULL, $level = 0) {
		
		$branch = [];
	    if(count($elements) == 0) {
	    	$elements = Group::model()->findAll("is_deleted = :is_deleted", [ ':is_deleted' => 0 ]);
	    } 

	   foreach ($elements as $element) {
	   		$el = [];
	        if ($element->parent_id === $parent_id) {
	        	
	        	$el['name'] = $element->name;
	        	$el['id'] = $element->id;
	        	$el['level'] = $level;
	        	$el['parent_id'] = $element->parent_id;
	        	$level++;
	            $children = $this->getGroupsArray($elements, $element->id, $level);

	            if ($children) {
	                $el['children'] = $children;
	            }

	            $branch[] = $el;
	            $level--;
	        }
	    }
	    	
	        return $branch; 
	}

	/**
	 * return group ar children elements
	 * @return array 
	 */
	public function getChildren(){	
		
		if(null === $this->_children){
			$this->_children = Group::model()->findAll("parent_id = :parent_id and is_deleted = :is_deleted", [ ':parent_id' => $this->id, ':is_deleted' => 0 ]);
		}
		return $this->_children;

	}

	/**
	 * Similar to getGroupsArray but return objects in array 
	 * @param  array   $elements groups objects
	 * @param  integer  $parent_id 
	 * @param  integer $level  
	 * @return array    return groups in hierarchy
	 */
	private function buildTree(array $elements = [], $parent_id = NULL) {
   
	    $branch = [];
	    if(count($elements) == 0) {
	    	$elements = Group::model()->findAll("is_deleted = :is_deleted", [ ':is_deleted' => 0 ]);
	    } 

	   foreach ($elements as $element) {
	        if ($element->parent_id === $parent_id) {
	            $children = $this->buildTree($elements, $element->id);
	            if ($children) {
	                $element->_children = $children;
	            }
	            $branch[] = $element;
	        }
	    }
	        return $branch; 
	    }
}
