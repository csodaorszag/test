<?php
return array (
  'ManageGroups' => 
  array (
    'type' => 0,
    'description' => 'Manage groups',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'student' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'assignments' => 
    array (
      'studentuser' => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
    ),
  ),
  'admin' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'ManageGroups',
    ),
    'assignments' => 
    array (
      'adminuser' => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
    ),
  ),
);
