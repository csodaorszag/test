<?php

class InitController extends Controller
{

	public function actionRun()
	{
		$auth=Yii::app()->authManager;
		$auth->createOperation('ManageGroups','Manage groups');

		$role=$auth->createRole('student');
		$role=$auth->createRole('admin');
		$role->addChild('ManageGroups');

		$auth->assign('student','studentuser');
		$auth->assign('admin','adminuser');
		// $auth->save();
		echo 'run';
	}

	public function actionCheck()
	{
		if(Yii::app()->user->checkAccess('ManageGroups'))
		{
		   echo('authorized');
		} else {
			echo('unauthorized');
		}
	}

}